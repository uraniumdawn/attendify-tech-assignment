(ns com.petrovsky.attendify.techassignment.tests
  (:require [clojure.test :refer :all]
            [com.petrovsky.attendify.techassignment.core :refer :all]))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= (url-recognizer "https://www.google.com.ua/search?q=desert&oq=desert&aqs=chrome..69i57j0j69i65j0l3.2138j1j7&sourceid=chrome&ie=UTF-8")
           {:URL "https://www.google.com.ua/search?q=desert&oq=desert&aqs=chrome..69i57j0j69i65j0l3.2138j1j7&sourceid=chrome&ie=UTF-8",
            :protocol "https",
            :host "www.google.com.ua",
            :port -1,
            :path "/search",
            :params {:aqs "chrome..69i57j0j69i65j0l3.2138j1j7", :q "desert", :ie "UTF-8", :sourceid "chrome", :oq "desert"}}))
    (is (= (url-recognizer "https://www.google.com.ua/search??q=desert&oq=desert&aqs=chrome..69i57j0j69i65j0l3.2138j1j7&sourceid=chrome&ie=UTF-8") nil))
    (is (= (url-recognizer "https://www.google.com.ua/search?q==desert&oq=desert&aqs=chrome..69i57j0j69i65j0l3.2138j1j7&sourceid=chrome&ie=UTF-8") nil))
    (is (= (url-recognizer "https://www.google.com.ua/search//") nil))
    (is (= (url-recognizer "https://www.google.com.ua/search/user/4566")
           {:URL "https://www.google.com.ua/search/user/4566",
            :protocol "https",
            :host "www.google.com.ua",
            :port -1,
            :path "/search/user/4566",
            :params {}}))
    (is (= (url-recognizer "https://www.google.com.ua:378")
           {:URL "https://www.google.com.ua:378",
            :protocol "https",
            :host "www.google.com.ua",
            :port 378,
            :path "",
            :params {}}))
    (is (= (url-recognizer "https://www.google.com.ua")
           {:URL "https://www.google.com.ua",
            :protocol "https",
            :host "www.google.com.ua",
            :port -1,
            :path "",
            :params {}}))))

