(defproject attendify-tech-assignment "0.1.0-SNAPSHOT"
  :description "Simple Clojure functions for recognition URL"
  :license {:name "Beeware License"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main ^:skip-aot com.petrovsky.attendify.techassignment.core
  :target-path "target/%s"
  :jar-name "attendify-tech-assignment.jar"
  :jar-path "/target/attendify-tech-assignment.jar"
  :profiles {:uberjar {:aot :all}})
