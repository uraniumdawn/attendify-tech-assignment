(ns com.petrovsky.attendify.techassignment.core
  (:require [clojure.string :as string])
  (:require [clojure.walk :as walk])
  (:gen-class))
(import java.net.URL)
(import java.net.MalformedURLException)

;; Pattern doesn't cover all possible views of URL
(def URL_PATTERN #"^ftp|https?:\/\/[a-zA-Z0-9\-.]+:?[0-9]*(?:[a-zA-Z0-9\-.]\/[a-zA-Z0-9\-.]*)*(?:\??[a-zA-Z0-9]+=[a-zA-Z0-9\-._]+&?)*$")

(defn params-to-map
  [params]
  (walk/keywordize-keys
    (apply hash-map
           (string/split params #"(&|=)"))))
(defn is-URL-valid [url] (if (nil? (re-matches URL_PATTERN url)) (throw (MalformedURLException.))))

(defn extract-details [url] {:URL      (.toString url)
                             :protocol (.getProtocol url)
                             :host     (.getHost url)
                             :port     (.getPort url)
                             :path     (.getPath url)
                             :params   (let [params (.getQuery url)] (if (nil? params) {} (params-to-map params)))})

(defn url-recognizer [url] (try (let [u (URL. url)] (is-URL-valid url) (extract-details u))
                                (catch MalformedURLException e (println "URL is invalid. Can't extract details from URL"))))

(defn -main [& args])
